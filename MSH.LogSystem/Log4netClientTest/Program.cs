﻿using MSH.LogClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Log4netClientTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SendError();
            //SendDebug();
            //SendInfo();
            //SendWarn();
            Console.ReadKey();
        }

        private static void SendError()
        {
            Task.Run(() =>
            {
                for (int i = 0; i < 100000; i++)
                {
                    MSHLogger.Instance("测试业务1", "测试业务2").Error("测试的Error日志信息");
                }
            });
        }

        private static void SendDebug()
        {
            Task.Run(() =>
            {
                for (int i = 0; i < 3; i++)
                {
                    MSHLogger.Instance("测试业务1", "测试业务2").Debug("测试的Error日志信息");
                }
            });
        }

        private static void SendInfo()
        {
            Task.Run(() =>
            {
                for (int i = 0; i < 1000; i++)
                {
                    MSHLogger.Instance("测试业务1", "测试业务2").Info("测试的Error日志信息");
                }
            });
        }

        private static void SendWarn()
        {
            Task.Run(() =>
            {
                for (int i = 0; i < 1000; i++)
                {
                    MSHLogger.Instance("测试业务1", "测试业务2").Warn("测试的Error日志信息");
                }
            });
        }
    }
}
