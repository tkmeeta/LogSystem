﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    /// <summary>
    /// 日志转发规则
    /// </summary>
    public class ForwardingRule
    {
        /// <summary>
        /// 匹配字段
        /// </summary>
        public string KeyName { get; set; }
        /// <summary>
        /// 匹配值
        /// </summary>
        public string KeyValue { get; set; }
        /// <summary>
        /// 匹配规则
        /// </summary>
        public string Rule { get; set; }
    }
}
