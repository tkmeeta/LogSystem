﻿using DTO;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSH.LogClient;
using System.Threading;

namespace ConsoleClientTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //SendError();
            //SendDebug();
            //SendInfo();
            SendWarn();
            Console.ReadKey();
        }

        private static void SendError()
        {
            Task.Run(() =>
            {
                var guid = Guid.NewGuid().ToString();
                for (int i = 0; i < 1000; i++)
                {
                    MSHLogger.Instance("测试业务1", "测试业务2").SetRequestId($"{guid}").Error("测试的Error日志信息");
                    Thread.Sleep(500);
                }
            });
        }

        private static void SendDebug()
        {
            Task.Run(() =>
            {
                var guid = Guid.NewGuid().ToString();
                for (int i = 0; i < 3; i++)
                {
                    MSHLogger.Instance("测试业务1", "测试业务2").SetRequestId($"{guid}").Debug("测试的Error日志信息");
                }
            });
        }

        private static void SendInfo()
        {
            Task.Run(() =>
            {
                var guid = Guid.NewGuid().ToString();
                for (int i = 0; i < 20; i++)
                {
                    MSHLogger.Instance("测试业务1", "测试业务2").SetRequestId($"{guid}").Info("测试的Error日志信息");
                }
            });
        }

        private static void SendWarn()
        {
            Task.Run(() =>
            {
                var guid = Guid.NewGuid().ToString();
                for (int i = 0; i < 1000; i++)
                {
                    MSHLogger.Instance("测试业务1", "测试业务2").SetRequestId($"{guid}").Warn("测试的Error日志信息");
                }
            });
        }
    }
}
