﻿using BusinessLayer.Interface;
using Common;
using Configuration;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiService.Core.ApiControllers
{
    /// <summary>
    /// 日志设置管理
    /// </summary>
    public class LogSettingController : ManagerController
    {
        public IPlatformManager IPlatformManager { get; set; }

        /// <summary>
        /// 配置钉钉微应用
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        [HttpPut]
        public AjaxReturnInfo ConfigDingtalkWeApp(DingtalkWeAppConfig config)
        {
            return new AjaxReturnInfo();
        }

        /// <summary>
        /// 配置日志转发规则
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public AjaxReturnInfo ConfigForwardingRules()
        {
            return new AjaxReturnInfo();
        }
    }
}
