﻿using BusinessLayer.Interface;
using DTO;
using MongoDbAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ReportManager : IReportManager
    {
        DebugLogAccess _DebugLogAccess = new DebugLogAccess();
        ErrorLogAccess _ErrorLogAccess = new ErrorLogAccess();
        InfoLogAccess _InfoLogAccess = new InfoLogAccess();
        WarnLogAccess _WarnLogAccess = new WarnLogAccess();

        public LogAddCountReport GetLogAddCountReport()
        {
            var report = new LogAddCountReport();
            report.TodayDebugCount = _DebugLogAccess.GetTodayCount();
            report.TodayErrorCount = _ErrorLogAccess.GetTodayCount();
            report.TodayInfoCount = _InfoLogAccess.GetTodayCount();
            report.TodayWarnCount = _WarnLogAccess.GetTodayCount();
            return report;
        }

        public TotalLogReport GetTotalLogReport()
        {
            var report = new TotalLogReport();
            report.DebugCount = _DebugLogAccess.GetTotalCount();
            report.ErrorCount = _ErrorLogAccess.GetTotalCount();
            report.InfoCount = _InfoLogAccess.GetTotalCount();
            report.WarnCount = _WarnLogAccess.GetTotalCount();
            return report;
        }
    }
}
